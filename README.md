# Interject Interview Projects

This repo contains a number of projects focused on different engineering topics for Interject applicants to demonstrate their technical abilities.

<br>
<br>
<br>

# Projects
### Financial Data Engineering

[LINK: financial-data-engineering](./financial-data-engineering/readme.md)

In this project you will build a SQL database and ingest financial and stock data into your system to ask questions about the performance of specific company over the last year. 


# Financial Data Engineering
This project is focused on using public data from multiple sources to build a groundwork database for analyzing and exploring securities similar to how an investment firm might do.

The goal in this project is to combine data about a fictional energy fund's holdings with data from the SEC about securities and then build a small reporting app to search our holdings by SEC Accession Number. The `holdings.csv` dataset only has ticker symbols, not security issue names, but the `yfinance` python package has security information that can be looked up via ticker symbol. This provides a tool for tying the two datasets together.

This project has no 1 right way of doing things, the goal is to make a best effort solution. Have fun and feel free to explore what makes the most sense to you!
- A) tie unrelated datasets together
- B) design a sensible database for storing the data
- C) develop a method to query the database for information that has been ingested.

<br>
<br>


### Resources
- Financials - https://www.sec.gov/dera/data/financial-statement-data-sets.html
    - Explaination of fields in SEC data - https://www.sec.gov/files/aqfs.pdf
- Exxon Mobil Stock Data
    - https://www.marketwatch.com/investing/stock/xom/download-data
    - https://finance.yahoo.com/quote/XOM/history?p=XOM
- yfinance - https://pypi.org/project/yfinance/
- SQL Server Express - https://www.microsoft.com/en-us/sql-server/sql-server-downloads
- project specific security holdings dataset - `data/holdings.csv`
- simple python based webapp framework - https://streamlit.io/
- python package for working with tabular datasets - https://pandas.pydata.org/

<br>
<br>


# Project Requirements
Choose a few of the following questions to answer and use the general requirements list below to design a data system that can anwser the reporting questions.

### Objectives
- be able to lookup a stock's price history from the database using its accession number
- be able to view entire holdings with stock company details and current portfolio value
- be able to lookup all stocks financials based on the SEC accession number.

<br>

### Suggested Solutions
- extract and combine data about comany and security info from the `sub.txt` file, yahoo stock history data and possibly the `yfinance` python package
- design a scheme for and ingest munged data into a SQL database of your choice
- use streamlit (or your choice of reporting interface) to pull and display data from your database: https://streamlit.io/
- (bonus) handle ingestion of data into the database via a stored procedure instead of entirely on the python side.
